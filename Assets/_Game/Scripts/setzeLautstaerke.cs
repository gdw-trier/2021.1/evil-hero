using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setzeLautstaerke : MonoBehaviour
{
    [SerializeField] private Lautstaerke lautstaerke;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource?>();
    }

    private void Update()
    {
        audioSource.volume = lautstaerke.Lautstärke;
    }

    private void OnDisable()
    {
        lautstaerke.Lautstärke = audioSource.volume;
    }
    private void OnDestroy()
    {
        lautstaerke.Lautstärke = audioSource.volume;
    }

    // public void setLautstaerke()
    // {
    //     lautstaerke.Lautstärke = gameObject.GetComponent<Slider>().value;
    // }
}
