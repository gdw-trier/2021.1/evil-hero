using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TriggerCollectible : MonoBehaviour
{
    [SerializeField] private PlayerStats playerStats;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private float moneyForCollectible = 5.0f;
    [SerializeField] private Animator anim;
    [SerializeField] private Sprite dinoSad;
    [SerializeField] private Sprite dinoHappy;
    private SpriteRenderer sr;

    private void Awake()
    {
        moneyText.gameObject.SetActive(false);
        TryGetComponent(out sr);
        sr.sprite = dinoSad;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out ICollect collect))
        {
            collect.OnCollect(playerStats, moneyText);
            sr.sprite = dinoHappy;
            moneyText.gameObject.SetActive(true);
            anim.SetTrigger("moneyTrigger");
            Destroy(other.gameObject);
        }
    }
}
