using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private CinemachineVirtualCamera cmCamera;
    private float shakeTimer = 0;
    private float shakeTimerTotal = 0;
    private float startingIntensity = 0;
    private CinemachineBasicMultiChannelPerlin cmNoise;
    
    public static CameraShake Instance { get; private set; } 
    

    private void Awake()
    {
        Instance = this;
        TryGetComponent(out cmCamera);
        cmNoise = cmCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        shakeTimerTotal = 0;
        shakeTimer = 0;
        startingIntensity = 0;
    }

    public void ShakeCamera(float intensity, float time)
    {
        cmNoise.m_AmplitudeGain = intensity;
        shakeTimerTotal = time;
        shakeTimer = time;
        startingIntensity = intensity;

    }

    private void Update()
    {
        if (shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;

            if (shakeTimer <= 0)
            {
                cmNoise.m_AmplitudeGain = Mathf.Lerp(startingIntensity, 0.0f, 1-(shakeTimer / shakeTimerTotal));
            }
        }
    }
}
