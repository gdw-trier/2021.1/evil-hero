using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] private GameObject coinPrefab;
    [SerializeField] private int maxAmount;
    [SerializeField] private float spawnChance;


    private void Awake()
    {
        for (int i = 0; i < maxAmount; i++)
        {
            if (Random.Range(0.0f, 1.0f) <= spawnChance)
            {
                Vector2 rndPos = new Vector2(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f));
                Transform coinTran = Instantiate(coinPrefab, Vector2.zero, quaternion.identity, gameObject.transform).transform;
                coinTran.localPosition = rndPos;
            }
        }
    }
}
