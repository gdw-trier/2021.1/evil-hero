using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyCollect : MonoBehaviour, ICollect
{
    [SerializeField] private float moneyOnCollect;
    public void OnCollect(PlayerStats playerStats, TextMeshProUGUI displayElement)
    {
        displayElement.text = "+$";
        playerStats.PlayerMoney += moneyOnCollect;
    }
}
