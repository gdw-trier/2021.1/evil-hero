using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerCollect : MonoBehaviour
{
    [SerializeField] private PlayerStats playerStats;
    [SerializeField] private TextMeshProUGUI displayText;
    [SerializeField] private Animator anim;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out ICollect collect))
        {
            collect.OnCollect(playerStats, displayText);
            
            displayText.gameObject.SetActive(true);
            anim.SetTrigger("moneyTrigger");
            Destroy(other.gameObject);
        }
    }
}
