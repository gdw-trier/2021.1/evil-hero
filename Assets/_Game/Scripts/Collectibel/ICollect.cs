using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public interface ICollect 
{
    public void OnCollect(PlayerStats playerStats, TextMeshProUGUI displayElement);
}
