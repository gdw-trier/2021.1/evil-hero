using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FameCollect : MonoBehaviour, ICollect
{
    [SerializeField] private float fameOnCollect;
    public void OnCollect(PlayerStats playerStats, TextMeshProUGUI displayElement)
    {
        displayElement.text = "+ " +  fameOnCollect + " Fame";
        playerStats.PlayerFame += fameOnCollect;
    }
}
