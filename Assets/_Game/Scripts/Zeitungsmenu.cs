using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Zeitungsmenu : MonoBehaviour
{
    [SerializeField] private GameObject GritList;
    // [SerializeField] private GridLayoutGroup layoutGroup;
    [SerializeField] private CrimeActivHolder purchasableCrimes;
    [SerializeField] private CrimeActivHolder activCrimes;
    [SerializeField] private GameObject button;
    private ObservableList<Button> buttonList;
    [SerializeField] private PlayerStats player;
    [SerializeField] private ColorBlock orgColors;
    private Renderer rend;

    [SerializeField] private Image buttonBackgroundImage;
    [SerializeField] private TextMeshProUGUI geldAnzeige;
    [SerializeField] private Button backButton;
    [SerializeField] private Sprite backImage;
    [SerializeField] private Sprite checkMarkImage;
    
    public void CloseNewsPaper()
    {
        SceneManager.UnloadSceneAsync("ZeitungKaufMenu");
    }

    // Start is called before the first frame update
    void Start()
    {
        if (activCrimes.Crimes.Count > 0)
        {
            backButton.image.sprite = checkMarkImage;
        }
        else
        {
            backButton.image.sprite = backImage;
        }
        geldAnzeige.text = "Capeman: " + player.PlayerMoney.ToString("n2") + "$";
        buttonList = new ObservableList<Button>();
        foreach (var crime in purchasableCrimes.Crimes)
        {
            Button b = Instantiate(button, GritList.transform).GetComponent<Button>();
            //b.colors = orgColors;
            buttonList.Add(b);
            b.GetComponentInChildren<TextMeshProUGUI>().text = crime.CrimeName + "\n \n" + "Cost: " + crime.PurchaseCost + "$" + "\nEarnings Lvl: " + crime.MoneyOnSuccess / 100 + "\n \n" + crime.CrimeArticleDescription;
            if (crime.PurchaseCost > player.PlayerMoney && !activCrimes.Crimes.Contains(crime))
            {
                b.interactable = false;
            }

            if (activCrimes.Crimes.Contains(crime))
            {
                ColorBlock cb = new ColorBlock();
                cb.normalColor = new Color32(255, 255, 200,255);
                cb.selectedColor = new Color32(255, 255, 200, 255);
                cb.pressedColor = new Color(255*0.6f/255, 255*0.6f/255, 200*0.6f/255, 1);
                cb.highlightedColor = new Color(255 * 0.8f/255, 255 * 0.8f/255, 200 * 0.8f/255, 1);
                cb.colorMultiplier = 1;
                b.colors = cb;
            }
            b.onClick.AddListener(() => PurchaseCrime(crime, b) );
        }
    }

    private void PurchaseCrime(Crime crime, Button b)
    {
        if (!activCrimes.Crimes.Contains(crime))
        {
            player.PlayerMoney -= crime.PurchaseCost;
            activCrimes.AddCrime(crime); 
            //b.image = buttonBackgroundImage;
            ColorBlock cb = new ColorBlock();
            cb.normalColor = new Color32(255, 255, 200,255);
            cb.selectedColor = new Color32(255, 255, 200, 255);
            cb.pressedColor = new Color(255*0.6f/255, 255*0.6f/255, 200*0.6f/255, 1);
            cb.highlightedColor = new Color(255 * 0.8f/255, 255 * 0.8f/255, 200 * 0.8f/255, 1);
            cb.colorMultiplier = 1;
            b.colors = cb;
            //b.GetComponent<CanvasRenderer>().SetColor(new Color(105, 255, 106));
            Debug.Log(b.gameObject.name);

        }
        else
        {
            player.PlayerMoney += crime.PurchaseCost;
            activCrimes.RemoveCrime(crime);
            b.colors = orgColors;
            Debug.Log("blabla");
        }
        geldAnzeige.text = "Capeman: " + player.PlayerMoney.ToString("n2") + "$";

        //Buttons werden disabled, wenn man nicht genug Geld hat und den Crime nicht vorher ausgewählt hat
        for (int i = 0; i < buttonList.Count; i++)
        {
            if (purchasableCrimes.Crimes[i].PurchaseCost > player.PlayerMoney && !activCrimes.Crimes.Contains(purchasableCrimes.Crimes[i]))
            {
                buttonList[i].interactable = false;
            }
            else
            {
                 buttonList[i].interactable = true;
            }
                    
        }

        if (activCrimes.Crimes.Count > 0)
        {
            backButton.image.sprite = checkMarkImage;
        }
        else
        {
            backButton.image.sprite = backImage;
            Debug.Log("Backstreets back allright");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
