using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed = 10f;
    private Vector2 direction = Vector2.zero;
    private Rigidbody2D rb;
    private Animator anim;

    private void Awake()
    {
        TryGetComponent(out rb);
        TryGetComponent(out anim);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + direction * (Time.deltaTime * speed));
    }

    public void OnMovement(InputAction.CallbackContext value)
    {
        direction = value.ReadValue<Vector2>();
        anim.SetFloat("xVelo", direction.x);
        anim.SetFloat("yVelo", direction.y);
    }
}
