using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private float interactionRadius = 1.0f;

    private Transform playerTransform = null;

    private void Awake()
    {
        playerTransform = transform;
    }

    public void OnInteraction(InputAction.CallbackContext value)
    {
        if(!value.started)
            return;
        Collider2D[] results = new Collider2D[20];
        int hit = Physics2D.OverlapCircleNonAlloc(playerTransform.position, interactionRadius, results);

        if (hit < 1)
            return;
        

        for (int i = 0; i < results.Length; i++)
        {
            if(results[i] == null)
                continue;
            if (results[i].TryGetComponent<IInteractable>(out var toInteract))
            {
                toInteract.Interact(gameObject);
                return;
            }
        }
    }
}
