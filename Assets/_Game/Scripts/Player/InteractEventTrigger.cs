using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractEventTrigger : MonoBehaviour, IInteractable
{
    public UnityEvent unityEvent;
    public void Interact(GameObject actor)
    {
        unityEvent?.Invoke();
    }
}
