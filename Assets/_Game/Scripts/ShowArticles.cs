using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public class ShowArticles : MonoBehaviour
{
    [SerializeField] private GameObject article;
    [SerializeField] private GameObject gritlist;
    [SerializeField] private CrimeResultList crimes;

    private ObservableList<GameObject> articleList;
    // Start is called before the first frame update
    void Start()
    {
        articleList = new ObservableList<GameObject>();
        Debug.Log(crimes.crimes.Count);
        foreach (var crime in crimes.crimes)
        {
            GameObject go = Instantiate(article, gritlist.transform);
            Debug.Log(crime.Key.CrimeName);
            if (crime.Value)
            {
                go.GetComponentsInChildren<TextMeshProUGUI>()[0].text = crime.Key.CrimeHeadlineOnSuccess;
                go.GetComponentsInChildren<TextMeshProUGUI>()[1].text = crime.Key.CrimeArticleOnSuccess;
            }

            else
            {
                go.GetComponentsInChildren<TextMeshProUGUI>()[0].text = crime.Key.CrimeHeadlineOnFailure;
                go.GetComponentsInChildren<TextMeshProUGUI>()[1].text = crime.Key.CrimeArticleOnFailure;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
