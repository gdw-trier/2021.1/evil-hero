using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ticker : MonoBehaviour
{
    [SerializeField] int tickerSpeed = 1;
    [SerializeField] private GameObject tickerText;
    private RectTransform ttRectTransform;


    private void Start()
    {
        ttRectTransform = tickerText.GetComponent<RectTransform>();
    }

    void Update()
    {
        ttRectTransform.position = new Vector2(ttRectTransform.position.x - 1  * Time.deltaTime * tickerSpeed, ttRectTransform.position.y);

        if (ttRectTransform.anchoredPosition.x <= - Screen.width - ttRectTransform.rect.width) {
            ttRectTransform.anchoredPosition = new Vector2(0, 0);
        }
    }
}
