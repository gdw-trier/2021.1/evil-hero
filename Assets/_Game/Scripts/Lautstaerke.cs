using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "LautstaerkeObjekt")]
public class Lautstaerke : ScriptableObject
{
    [SerializeField] private float lautstärke = 1;

    public float Lautstärke
    {
        get => lautstärke;
        set => lautstärke = value;
    }

}
