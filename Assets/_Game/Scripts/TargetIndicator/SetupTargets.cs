using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class SetupTargets : MonoBehaviour
{
   [SerializeField] private GameObject indicatorPrefab;
   [SerializeField] private IndicatorEvent indicatorEvent;
   private Dictionary<Crime, TargetIndicator> dictOfIndicators = new Dictionary<Crime, TargetIndicator>();

   private void Awake()
   {
      indicatorEvent.OnIndicatorsRequested += HandleIndicator;
   }

   public void HandleIndicator(Crime crime, Transform targetPos, bool isAktiv, Color indicatorColor)
   {
      if (isAktiv)
      {
         TargetIndicator indicator = Instantiate(indicatorPrefab, transform.position, quaternion.identity, gameObject.transform)
            .GetComponent<TargetIndicator>();
         indicator.SetTarget(targetPos, indicatorColor);
         dictOfIndicators.Add(crime, indicator);
      }
      else
      {
         if (dictOfIndicators.ContainsKey(crime))
         {
            dictOfIndicators[crime].DisableIndicator();
         }
      }
   }
   
   private void OnDestroy()
   {
      indicatorEvent.OnIndicatorsRequested = null;
   }
   
}
