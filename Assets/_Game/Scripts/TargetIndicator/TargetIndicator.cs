using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour
{
    private Transform target;
    [SerializeField] private float hideDistance = 3;
    [SerializeField] SpriteRenderer renderer;
    private Color indicatorColor = Color.white;
    private bool isDisabled = false;

    private void Awake()
    {
        renderer.color = indicatorColor;
        isDisabled = false;
    }

    private void Update()
    {
        if(isDisabled)
            return;
        var dir = target.position - transform.position;

        if (dir.magnitude <= hideDistance)
        {
            HideIndicator(true);
        }
        else
        {
            HideIndicator(false);
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public void DisableIndicator()
    {
        HideIndicator(true);
        isDisabled = true;
    }

    private void HideIndicator(bool toHide)
    {
        renderer.color = toHide ? Color.clear : indicatorColor;
    }
    
    public void SetTarget(Transform toTarget, Color colorForIndicator)
    {
        indicatorColor = colorForIndicator;
        target = toTarget;
    }
}

    