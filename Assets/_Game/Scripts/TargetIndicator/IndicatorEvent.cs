using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Event/IndicatorEvent")]
public class IndicatorEvent : ScriptableObject
{
    public UnityAction<Crime, Transform, bool, Color> OnIndicatorsRequested;

    public void RaiseEvent(Crime crime, Transform position, bool isActive, Color indicatorColor)
    {
        OnIndicatorsRequested?.Invoke(crime, position, isActive, indicatorColor);
    }
}
