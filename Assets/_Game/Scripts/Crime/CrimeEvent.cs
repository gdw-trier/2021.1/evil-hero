using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Event/CrimeEvent")]
public class CrimeEvent : ScriptableObject
{
    public UnityAction<List<Crime>> OnCrimesRequested;

    public void RaiseEvent(List<Crime> crimes)
    {
        OnCrimesRequested?.Invoke(crimes);
    }
}
