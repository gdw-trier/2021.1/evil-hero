using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CrimePlace : MonoBehaviour, IInteractable
{
    [SerializeField] private Crime crime;
    [Header("Events")]
    [SerializeField] private CrimeEvent crimeEvent;
    [SerializeField] private MiniGameEvent minigameEvent;
    [SerializeField] private SceneLoadEvent loadMiniGameEvent;
    [SerializeField] private CrimeResultList crimeResults;
    [SerializeField] private IndicatorEvent indicatorEvent;
    [SerializeField] private TimeEndDistributor timeEndEvent;
    
    [Header("Helper")]
    [SerializeField] private PlayerStats playerStats;
    [SerializeField] private CrimeActivHolder currentCrime;
    [SerializeField] private Image image;
    [SerializeField] private Image resultImage;
    [SerializeField] private Sprite successSprite;
    [SerializeField] private Sprite loseSprite;
    [SerializeField] private TextMeshProUGUI fameText;
    [SerializeField] private TextMeshProUGUI moneyText;
    
    
    
    private bool isActiv = false;
    private bool isSceneLoaded = false;

    private void Awake()
    {
        isActiv = false;
        image.color = Color.clear;
        crimeEvent.OnCrimesRequested += InitPlace;
        resultImage.gameObject.SetActive(false);
        fameText.gameObject.SetActive(false);
        moneyText.gameObject.SetActive(false);
    }

    private void InitPlace(List<Crime> crimes)
    {
        if (crimes.Contains(crime))
        {
            isActiv = true;
            image.color = Color.white;
            indicatorEvent.RaiseEvent(crime, transform, true, crime.CrimeColor);
            timeEndEvent.OnTimerEndRequested += OnEndTimer;
        }
    }

    public void Interact(GameObject actor)
    {
        if(isActiv && !isSceneLoaded)
           StartCrime();
    }

    public void StartCrime()
    {
        minigameEvent.OnMiniGameRequested = OnEndCrime;
        loadMiniGameEvent.RaiseEvent(crime.MiniGameSceneName);
        isSceneLoaded = true;
        image.color = Color.clear;
    }

    public void OnEndCrime(bool success, float multiplier)
    {
        resultImage.gameObject.SetActive(true);
        fameText.gameObject.SetActive(true);
        moneyText.gameObject.SetActive(true);
        resultImage.sprite = success ? successSprite : loseSprite;
        
        indicatorEvent.RaiseEvent(crime, transform, false, crime.CrimeColor);
        isSceneLoaded = false;
        isActiv = false;
        minigameEvent.OnMiniGameRequested = null;
        timeEndEvent.OnTimerEndRequested -= OnEndTimer;
        crimeResults.AddCrime(crime, success);
        
        if (success)
        {
            playerStats.PlayerMoney += crime.MoneyOnSuccess * multiplier * Mathf.Clamp(currentCrime.Crimes.Count/2.0f, 1.0f, 10.0f);
            playerStats.PlayerFame += crime.FameOnSuccess;

            moneyText.text = "+ " +  (crime.MoneyOnSuccess * multiplier * Mathf.Clamp(currentCrime.Crimes.Count/2.0f, 1.0f, 10.0f)).ToString("n2") + "$";
            fameText.text = "+ " + crime.FameOnSuccess + " Fame";;
            return;
        }

        playerStats.PlayerFame -= crime.FameOnFailure;
        
        moneyText.text = "+ " +  0 + "$";
        fameText.text = "- " + crime.FameOnFailure + " Fame";
    }

    public void OnEndTimer()
    {
        indicatorEvent.RaiseEvent(crime, transform, false, crime.CrimeColor);
        isSceneLoaded = false;
        isActiv = false;
        minigameEvent.OnMiniGameRequested = null;
        timeEndEvent.OnTimerEndRequested -= OnEndTimer;
        crimeResults.AddCrime(crime, false);
        playerStats.PlayerFame -= crime.FameOnFailure;
    }

    private void OnDestroy()
    {
        crimeEvent.OnCrimesRequested = null;
        minigameEvent.OnMiniGameRequested = null;
        timeEndEvent.OnTimerEndRequested -= OnEndTimer;
    }
}
