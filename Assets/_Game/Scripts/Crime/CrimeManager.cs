using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrimeManager : MonoBehaviour
{
    [SerializeField] private CrimeActivHolder crimeHolder;
    [SerializeField] private CrimeResultList crimeResults;
    [SerializeField] private CrimeEvent crimeEvent;
    [SerializeField] private PlayerStats playerStats;
    [SerializeField] private SceneChanger sceneChanger;
    [SerializeField] private MiniGameEvent miniGameEvent;
    [SerializeField] private TimeEndDistributor timeEndEvent;
    [SerializeField] private Animator playerAnim;
    [SerializeField] private RuntimeAnimatorController normalSkin;
    [SerializeField] private AnimatorOverrideController tigerSkin;
    
    private void Start()
    {
        SetSkin();
        Debug.Log("Start?");
        crimeResults.ClearCrimes();
        crimeEvent.RaiseEvent(crimeHolder.Crimes);
        playerStats.InitFameForDiff();
    }


    public void OnTimerEnd()
    {
        timeEndEvent.RaiseEvent();
        crimeHolder.ClearCrimes();
        if(playerStats.HasLostToMuchFame() || playerStats.PlayerFame < 0.0f || playerStats.PlayerMoney < 100.0f)
            sceneChanger.LoadScene("EndScreen");
        else
            sceneChanger.LoadScene("BaseTestScene");    
    }

    public void OnDestroy()
    {
        timeEndEvent.OnTimerEndRequested = null;
    }

    private void SetSkin()
    {
        playerAnim.runtimeAnimatorController = playerStats.PlayerFame > 150.0f ? tigerSkin as RuntimeAnimatorController : normalSkin;
    }
}
