using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Crime/Crime Holder")]
public class CrimeActivHolder : ScriptableObject
{
    //Überlegen ob Crime aktiv mit abspeichern
    [SerializeField] private List<Crime> crimes = new List<Crime>();

    public List<Crime> Crimes
    {
        get => crimes;
        set => crimes = value;
    }

    public void AddCrime(Crime crime)
    {
        crimes.Add(crime);
    }

    public void ClearCrimes()
    {
        crimes.Clear();
    }

    public void RemoveCrime(Crime crime)
    {
        crimes.Remove(crime);
    }
}
