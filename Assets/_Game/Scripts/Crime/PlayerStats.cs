using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Player")]
public class PlayerStats : ScriptableObject
{
    [SerializeField] private float playerMoney = 200.0f;
    [SerializeField] private float playerFame = 100.0f;
    [SerializeField] private float fameDiffForLose = 10;
    private float lastFame = 100.0f;

    public float PlayerMoney
    {
        get => playerMoney;
        set => playerMoney = value;
    }

    public float PlayerFame
    {
        get => playerFame;
        set => playerFame = value;
    }

    public float FameDiffForLose => fameDiffForLose;

    public void InitFameForDiff()
    {
        lastFame = playerFame;
    }

    private float GetFameDiff()
    {
        return playerFame - lastFame;
    }

    public bool HasLostToMuchFame()
    {
        if(lastFame > playerFame)
            return Mathf.Abs(lastFame - playerFame) >= fameDiffForLose;
        return false;
    }
}
