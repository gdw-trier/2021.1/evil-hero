using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Crime/Crime ResultHolder")]
public class CrimeResultList : ScriptableObject
{
    public Dictionary<Crime, bool> crimes = new Dictionary<Crime, bool>();


    public void AddCrime(Crime crime, bool success)
    {
        Debug.Log($"crime: {crime}");
        crimes.Add(crime, success);
    }

    public void ClearCrimes()
    {
        crimes.Clear();
    }

    public void RemoveCrime(Crime crime)
    {
        crimes.Remove(crime);
    }

    public void Reset(){
        Debug.Log("Hallo ich mache was, was ich nicht soll!");
    }

    public void OnValidate()
    {
        Debug.Log($"Hier wird was validiert : {crimes.Keys.Count}");
    }
}
