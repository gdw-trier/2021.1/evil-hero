using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Crime/Create Crime")]
public class Crime : ScriptableObject
{
  [SerializeField] private Color crimeColor = Color.white;
  [SerializeField] private string crimeName;
  [SerializeField][TextArea(3,10)] private string crimeArticleDescription;
  [SerializeField] private string miniGameSceneName;
  [SerializeField] private int moneyOnSuccess = 0;
  [SerializeField] private int fameOnSuccess = 0;
  [SerializeField] private int fameOnFailure = 0;
  [SerializeField] private int purchaseCost = 0;
  [SerializeField][TextArea(3,10)] private string crimeArticleOnSuccess;
  [SerializeField][TextArea(3,10)] private string crimeArticleOnFailure;
  [SerializeField] private string crimeHeadlineOnSuccess;
  [SerializeField] private string crimeHeadlineOnFailure;
  public Color CrimeColor => crimeColor;

  public string CrimeName => crimeName;

  public string CrimeArticleDescription => crimeArticleDescription;

  public string CrimeArticleOnSuccess => crimeArticleOnSuccess;

  public string CrimeArticleOnFailure => crimeArticleOnFailure;

  public int FameOnSuccess => fameOnSuccess;

  public int FameOnFailure => fameOnFailure;

  public int MoneyOnSuccess => moneyOnSuccess;

  public string MiniGameSceneName => miniGameSceneName;
  public int PurchaseCost => purchaseCost;
  
  public string CrimeHeadlineOnSuccess => crimeHeadlineOnSuccess;

  public string CrimeHeadlineOnFailure => crimeHeadlineOnFailure;
}
