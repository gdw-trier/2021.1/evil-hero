using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanGoToCity : MonoBehaviour
{
    [SerializeField] private GameObject sceneChanger;

    [SerializeField] private CrimeActivHolder acrivecrimes;
    // Start is called before the first frame update
    public void AreThereCrimesToFight()
    {
        if (acrivecrimes.Crimes.Count > 0)
        {
            sceneChanger.GetComponent<SceneChanger>().LoadScene("CityScene");
        }
        //nice to have: ein Schriftzug, der sagt, dass es keine Active Crimes gibt.
    }
}
