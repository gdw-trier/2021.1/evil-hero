using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InitEnd : MonoBehaviour
{
    [SerializeField] private Sprite winSprite;
    [SerializeField] private Sprite loseSprite;
    [SerializeField] private PlayerStats playerStats;

    [Header("UI")] 
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI headerText;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI highscoreText;
    
    
    private void Start()
    {
        if (playerStats.HasLostToMuchFame())
        {
            image.sprite = loseSprite;
            headerText.text = "Game Over!";
        }
        else
        {
            image.sprite = winSprite;
            headerText.text = "Everybody needs a pause";
        }

        moneyText.text = "Money: " + playerStats.PlayerMoney.ToString("n2") + "$";
        highscoreText.text = "Your Fame: " + playerStats.PlayerFame;
    }
}
