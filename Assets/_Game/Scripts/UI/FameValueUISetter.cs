using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FameValueUISetter : MonoBehaviour
{
    [SerializeField] private PlayerStats playerStats;
    private TextMeshProUGUI fameText;

    private void Awake()
    {
        TryGetComponent(out fameText);
        fameText.text = "Fame: " + playerStats.PlayerFame;

    }

    private void Update()
    {
        fameText.text = "Fame: " + playerStats.PlayerFame;
    }

 
}
