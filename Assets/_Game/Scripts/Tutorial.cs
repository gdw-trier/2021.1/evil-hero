using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private Sprite[] images;
    [SerializeField] private Button button;

    private int i = 0;
    // Start is called before the first frame update
    public void nextImage()
    {
        if (++i < images.Length)
        {
            GetComponentInChildren<Image>().sprite = images[i];
            if (i == images.Length - 1)
            {
                gameObject.GetComponentsInChildren<Button>()[0].gameObject.SetActive(false);
                button.gameObject.SetActive(true);
            }
        }
        else
        {
            
        }
    }

    public void firstImage()
    {
        i = 0;
        GetComponentInChildren<Image>().sprite = images[i];
    }
    
}
