using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Event/TimerEndDisEvent")]
public class TimeEndDistributor : ScriptableObject
{
    public UnityAction OnTimerEndRequested;

    public void RaiseEvent()
    {
        OnTimerEndRequested?.Invoke();
    }
}
