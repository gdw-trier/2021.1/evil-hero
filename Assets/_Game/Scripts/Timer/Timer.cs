using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private float timeInSeconds = 300;

    [SerializeField] private Image countdownBar;
    [SerializeField] private TextMeshProUGUI countdownText;

    private float currentTime = 0.0f;
    private int timeFlatSeconds = 0;
    private Color currentColor = new Color(0,1,0,1);
    private bool hasEvented = false;
    
    public UnityEvent timerEvent;

    public float CurrentTime => currentTime;

    public float TimeInSeconds => timeInSeconds;

    void Start()
    {
        currentTime = timeInSeconds;
        timeFlatSeconds = (int)currentTime;
        countdownBar.color = currentColor;
        countdownBar.fillAmount = 1.0f;
        hasEvented = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if(hasEvented)
            return;
        
        currentTime -= Time.deltaTime;
        timeFlatSeconds = (int)currentTime;

        if (currentTime <= 0.0f)
            currentTime = 0.0f;

        countdownText.text = (timeFlatSeconds / 60) + ":" + (timeFlatSeconds % 60).ToString("00") ;
        countdownBar.fillAmount = currentTime / timeInSeconds;
        
        if (countdownBar.fillAmount > 0.5f)
        {
            countdownBar.color = Color.Lerp(Color.yellow, Color.green, (countdownBar.fillAmount - 0.5f) * 2);
        }
        else if (countdownBar.fillAmount > 0.0f)
        {
            countdownBar.color = Color.Lerp(Color.red, Color.yellow, countdownBar.fillAmount * 2);
        }
        else
        {
            timerEvent?.Invoke();
            hasEvented = true;
        }
        
    }
}
