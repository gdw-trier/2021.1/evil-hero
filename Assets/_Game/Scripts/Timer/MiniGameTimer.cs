using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MiniGameTimer : MonoBehaviour
{
    [SerializeField] private float totalTimeInSeconds = 5.0f;
    private float currentTime = 0.0f;
    public UnityEvent onTimerEndEvent;
    private bool hasFinished = false;
    private bool hasStarted = false;
    private Slider slider;

    public void ResetTimer(float totalTime)
    {
        totalTimeInSeconds = totalTime;
        currentTime = totalTime;
        hasFinished = false;
        hasStarted = false;
        slider.maxValue = totalTime;
        slider.value = totalTime;
    }

    public float TotalTimeInSeconds => totalTimeInSeconds;

    public float CurrentTime => currentTime;

    public void StartTimer()
    {
        hasStarted = true;
    }

    private void Awake()
    {
        TryGetComponent(out slider);
    }

    private void Start()
    {
        ResetTimer(totalTimeInSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        if(hasFinished || !hasStarted)
            return;

        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            currentTime = 0.0f;
            hasFinished = true;
            onTimerEndEvent?.Invoke();
        }

        slider.value = currentTime;
    }
}
