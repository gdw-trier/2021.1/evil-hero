using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenZeitung : MonoBehaviour
{
    public void OpenNewsPaper()
    {
        SceneManager.LoadScene("ZeitungKaufMenu", LoadSceneMode.Additive);
    }
}
