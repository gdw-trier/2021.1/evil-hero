using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CatSpawnOnClick : MonoBehaviour
{
    [SerializeField] private GameObject catPrefab;
    [SerializeField] private SlideScript sliderScript;


    private void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame && sliderScript.CanSpawnCat)
        {
            Instantiate(catPrefab, Vector3.zero, Quaternion.identity, transform);
        }
    }
}
