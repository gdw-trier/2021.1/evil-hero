using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootCapeCrimeDude : MonoBehaviour
{
    private Rigidbody2D rb;

    private float thiefPos;
    private float wallPos;
    private float maxDist;
    private float multiply;

    public MiniGameManager mM;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        thiefPos = rb.position.x;
        wallPos = 10.7f;
        maxDist = wallPos - thiefPos;
        multiply = 0;
    }

    private void Start()
    {
        rb.velocity += Vector2.right;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("ShootCapeFailed"))
        {
            mM.MiniGameEnd(false,0);
            Debug.Log("Loose");
        }
        if (coll.gameObject.CompareTag("ShootCapeBall"))
        {
            multiply = 0.5f + (float)Mathf.Clamp(1f-(((float)rb.position.x - (float)thiefPos)/(float)maxDist),0f,0.5f);
            mM.MiniGameEnd(true, multiply);
            Debug.Log(multiply);
        }
    }
}
