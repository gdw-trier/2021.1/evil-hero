using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class CatClick : MonoBehaviour
{
    private int currentFall;
    private bool finished;
    private int catchedObjects;
    [SerializeField] private GameObject go;
    private Transform holder;
    [SerializeField] private int maxObj;
    [SerializeField] private float repeatRate;
    [SerializeField] private LayerMask mask;
    public MiniGameManager mM;
    
    
    

    void Start()
    {
        InvokeRepeating(nameof(SpawnObj),1f,repeatRate);
    }
    
    void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            Vector2 startPos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            RaycastHit2D hit = Physics2D.Raycast(startPos, Vector2.zero, 1, mask);
            if (hit.collider != null)
            {
                Destroy(hit.collider.gameObject);
                catchedObjects++;
            }
        }
        
        if (finished)
        {
            if (catchedObjects > 0)
            {
                float temp = (float) catchedObjects / (float) maxObj;
                mM.MiniGameEnd(true, temp);
                Debug.Log(temp);
            }
            if (catchedObjects <= 0)
            {
                mM.MiniGameEnd(false,0);
                Debug.Log("loose");
            }

        }

    }
    private void SpawnObj()
    {
        Vector2 spawnObj = new Vector2(0, 5.5f);
        spawnObj.x = Random.Range(-8, 8);
        GameObject temp = Instantiate(go, spawnObj, quaternion.identity,holder);
        currentFall++;
        if (currentFall >= maxObj)
        {
            CancelInvoke();
            StartCoroutine(WaitForFinish());
        }
    }
    IEnumerator WaitForFinish()
    {
        yield return new WaitForSeconds(4);
        finished = true;
    }
}
