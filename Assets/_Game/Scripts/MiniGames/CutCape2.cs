using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CutCape2 : MonoBehaviour
{
    private bool hitDetec;
    private bool done1;
    private bool done2;
    private bool timerNotStartet;

    [HideInInspector] public String currentEnd;
    
    private Vector2 startPos;
    private Vector2 endPos;

    public MiniGameManager mM;
    

    private LineRenderer lr;
    private GameObject goMouseColl;
    [SerializeField] private GameObject goMouseCollPref;
    [SerializeField] private GameObject LineRender;
    [SerializeField] private Transform holder;
    
    
    
    [SerializeField] private MiniGameTimer tim;
    
    [SerializeField] private LayerMask mask;
    [SerializeField] private Material mat;


    private void Awake()
    {
        lr = GetComponent<LineRenderer>();

        lr.enabled = false;
        hitDetec = false;
        done1 = false;
        done2 = false;
        timerNotStartet = true;
    }

    private void Start()
    {
        goMouseColl = Instantiate(goMouseCollPref,holder);
        goMouseColl.GetComponent<CutCapeFin>().cB = deactivateLn;
    }
    //compare y values
    void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {

            startPos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            RaycastHit2D hit = Physics2D.Raycast(startPos, Vector2.zero, 1, mask);
            if (hit.collider != null && done1 == false && hit.collider.CompareTag("CutCapeStart1"))
            {
                lr.enabled = true;
                hitDetec = true;
                goMouseColl.GetComponent<CutCapeFin>().currentEnd = "CutCapeDest1";
                currentEnd = "CutCapeDest1";
                if (timerNotStartet)
                {
                    tim.StartTimer();
                    timerNotStartet = false;
                }
            }
            if (hit.collider != null && done2 == false && hit.collider.CompareTag("CutCapeStart2"))
            {
                lr.enabled = true;
                hitDetec = true;
                goMouseColl.GetComponent<CutCapeFin>().currentEnd = "CutCapeDest2";
                currentEnd = "CutCapeDest2";
                if (timerNotStartet)
                {
                    tim.StartTimer();
                    timerNotStartet = false;
                }
            }

        }

        if (done1 && done2)
        {
            float temp = Mathf.Clamp(tim.CurrentTime / (tim.TotalTimeInSeconds/2),0f,1f);
            mM.MiniGameEnd(true,temp);
        }
        
        if (hitDetec && Mouse.current.IsPressed())
        {
            SetLineRendererPos();

        }
        
        if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            lr.enabled = false;
            hitDetec = false;
        }
        
    }

    private void deactivateLn()
    {
        if (currentEnd == "CutCapeDest1")
        {
            done1 = true;
            GameObject myLine = Instantiate(LineRender,holder);
            myLine.transform.position = startPos;
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = mat;
            lr.SetPosition(0, startPos);
            lr.SetPosition(1, Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
        }
        if (currentEnd == "CutCapeDest2")
        {
            done2 = true;
            GameObject myLine = Instantiate(LineRender,holder);
            myLine.transform.position = startPos;
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = mat;
            lr.SetPosition(0, startPos);
            lr.SetPosition(1, Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()));
        }
        lr.enabled = false;
        hitDetec = false;
    }


    private void SetLineRendererPos()
    {
        Vector3[] positions = new Vector3[2];
        Vector2 endPos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        positions[0] = new Vector3(startPos.x,startPos.y,0);
        positions[1] = new Vector3(endPos.x, endPos.y, 0);
        lr.SetPositions(positions);
        goMouseColl.GetComponent<Rigidbody2D>().position = endPos;
    }
    public void OnLoose()
    {
        mM.MiniGameEnd(false,0);
    }
}
