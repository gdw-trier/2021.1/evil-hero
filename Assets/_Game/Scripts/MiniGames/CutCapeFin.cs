using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutCapeFin : MonoBehaviour
{
    [HideInInspector] public String currentEnd;
    public delegate void callback();

    public callback cB;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("CutCapeDest1") && currentEnd == "CutCapeDest1")
        {
            cB?.Invoke();
        }
        if (coll.gameObject.CompareTag("CutCapeDest2") && currentEnd == "CutCapeDest2")
        {
            cB?.Invoke();
        }
        if (coll.gameObject.CompareTag("CutCapeDest3") && currentEnd == "CutCapeDest3")
        {
            cB?.Invoke();
        }
    }
}
