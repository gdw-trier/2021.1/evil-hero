using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionEvent : MonoBehaviour
{
    public UnityEvent unityEvent;

    private void OnCollisionEnter2D(Collision2D other)
    {
        unityEvent?.Invoke();
    }
}
