using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ShootCape : MonoBehaviour
{

    private bool hitDetec;
    private bool enableTrail;

    private float releaseDelay;
    private Vector2 firstPos;
    private GameObject trailMaker;
    private Rigidbody2D rbT;
    private TrailRenderer trT;
    [SerializeField] private float maxDragDistance = 2f;

    public MiniGameManager mM;

    private Rigidbody2D rb;
    private Rigidbody2D fixPointRB;
    private SpringJoint2D sj;
    private LineRenderer lr;
    private TrailRenderer tr;
    [SerializeField] private LayerMask mask;
    [SerializeField] private Material mat;
    [SerializeField] private GameObject trailMakerPref;
    [SerializeField] private Image img;
    
    private Animator anim;
    
    [SerializeField] private Transform holder;
    
    

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sj = GetComponent<SpringJoint2D>();
        lr = GetComponent<LineRenderer>();
        tr = GetComponent<TrailRenderer>();
        anim = GetComponent<Animator>();
        fixPointRB = sj.connectedBody;
        firstPos = rb.position;

        lr.enabled = false;
        tr.enabled = false;
        releaseDelay = 1 / (sj.frequency * 4);

        trailMaker = Instantiate(trailMakerPref,holder);
        trT = trailMaker.GetComponent<TrailRenderer>();
        rbT = trailMaker.GetComponent<Rigidbody2D>();
        rbT.position = rb.position;
        enableTrail = false;
        trT.enabled = false;
    }

    void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            Vector2 startPos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            RaycastHit2D hit = Physics2D.Raycast(startPos, Vector2.zero, 1, mask);
            if (hit.collider != null)
            {
                hitDetec = true;
                rb.isKinematic = true;
                lr.enabled = true;
            }
        }

        if (Mouse.current.leftButton.isPressed)
        {
            if (hitDetec)
            {
                DragBall();
            }
        }

        if (enableTrail)
        {
            rbT.position = rb.position;
        }

        if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            if (hitDetec)
            {
                img.enabled = false;
                anim.SetTrigger("ShootBall");
                trT.enabled = false;
                rbT.position = rb.position;
                enableTrail = true;
                rb.isKinematic = false;
                lr.enabled = false;
                //tr.enabled = true;
                StartCoroutine(Release());
            }
            hitDetec = false;
        }
    }

    private void DragBall()
    {
        SetLineRendererPos();
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        float distance = Vector2.Distance(mousePos, fixPointRB.position);
        if (distance > maxDragDistance)
        {
            Vector2 direction = (mousePos - fixPointRB.position).normalized;
            rb.position = fixPointRB.position + direction * maxDragDistance;
        }
        else
        {
            rb.position = mousePos;
        }
        
    }

    private void SetLineRendererPos()
    {
        Vector3[] positions = new Vector3[2];
        positions[0] = rb.position;
        positions[1] = fixPointRB.position;
        lr.SetPositions(positions);
    }

    private IEnumerator Release()
    {
        yield return new WaitForSeconds(releaseDelay);
        trT.Clear();
        sj.enabled = false;
        trT.enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("ShootCapeFloor"))
        {
            anim.Play("capeBall_idle",-1);
            tr.enabled = false;
            tr.Clear();
            rb.position = firstPos;
            sj.enabled = true;
            enableTrail = false;
        }
    }
}
