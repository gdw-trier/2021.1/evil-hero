using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SlideScript : MonoBehaviour
{

    private Rigidbody2D rb;
    private Collider2D coll2D;
    private bool active;
    public MiniGameManager mM;
    public MiniGameTimer tim;

    public bool CanSpawnCat { get; private set; } = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        coll2D = GetComponent<Collider2D>();
        rb.position = new Vector2(-4.31f, 0.41f);
        active = true;
        CanSpawnCat = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Mouse.current.leftButton.isPressed && active)
        {
            rb.position = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        tim.StartTimer();
        coll2D.enabled = false;
        CanSpawnCat = true;
        active = false;
        rb.position = new Vector2(-1.15f,-0.56f);
    }

    public void ended()
    {
        Debug.Log("guht");
        mM.MiniGameEnd(true,1);
    }
}
