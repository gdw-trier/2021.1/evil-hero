using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class FallenPeopleScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private int catchedObjects;
    private bool finished;
    private int currentFall;
    [SerializeField] private Transform holder;
    [SerializeField] private float repeatRate;
    
    public MiniGameManager mM;
    [SerializeField] private GameObject go;
    
    [SerializeField] private int maxObj;
    

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        catchedObjects = 0;
        finished = false;
        currentFall = 0;
    }

    void Start()
    { 
        InvokeRepeating(nameof(SpawnObj),1f,repeatRate);

    }
    void Update()
    {
        rb.position = new Vector2(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()).x,rb.position.y);
        if (finished)
        {
            if (catchedObjects > 0)
            {
                float temp = (float) catchedObjects / (float) maxObj;
                mM.MiniGameEnd(true, temp);
                Debug.Log(temp);
            }
            if (catchedObjects <= 0)
            {
                mM.MiniGameEnd(false,0);
                Debug.Log("loose");
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        Destroy(coll.gameObject);
        catchedObjects++;
    }

    private void SpawnObj()
    {
        Vector2 spawnObj = new Vector2(0, 5.5f);
        spawnObj.x = Random.Range(-8, 8);
        GameObject temp = Instantiate(go, spawnObj, quaternion.identity,holder);
        currentFall++;
        if (currentFall >= maxObj)
        {
            CancelInvoke();
            StartCoroutine(WaitForFinish());
        }
    }

    private void FinishedFall()
    {
        finished = true;
    }

    IEnumerator WaitForFinish()
    {
        yield return new WaitForSeconds(4);
        finished = true;
    }
}
