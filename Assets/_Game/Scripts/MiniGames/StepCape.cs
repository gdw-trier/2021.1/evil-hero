using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

public class StepCape : MonoBehaviour
{
    public MiniGameManager mM;

    private float mousepos;
    private Rigidbody2D rb;
    private bool press;
    private bool ended;
    private int currentFall;
    private int maxObj;
    private float multiply;
    public MiniGameTimer tim;
    [SerializeField] private float capeStr;
    [SerializeField] private GameObject go;
    [SerializeField] private Transform holder;
    


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        mousepos = 0f;
        ended = false;
        currentFall = 0;
        maxObj = 60;
        press = false;
    }
    
    void Update()
    {
        float curMou = Mouse.current.position.ReadValue().x;
        if (Mouse.current.leftButton.isPressed && press == false)
        {
            mousepos = Mouse.current.position.ReadValue().x;
            press = true;
            tim.StartTimer();
        }

        if (Mouse.current.leftButton.wasReleasedThisFrame && ended == false)
        {
            curMou = Mouse.current.position.ReadValue().x;
            if (curMou > mousepos)
            {
                float temp = Mathf.Sqrt(Mathf.Pow((curMou - mousepos), 2))/capeStr;
                temp = rb.position.x + temp;
                rb.position = new Vector2(temp, rb.position.y);
            }

            press = false;
        }
        
    }
    private void SpawnObj()
    {
        Vector2 spawnObj = new Vector2(0, 5.5f);
        spawnObj.x = UnityEngine.Random.Range(-8, 8);
        GameObject temp = Instantiate(go, spawnObj, quaternion.identity,holder);
        currentFall++;
        if (currentFall >= maxObj)
        {
            CancelInvoke();
            StartCoroutine(WaitForFinish());
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        multiply = Mathf.Clamp(tim.CurrentTime / (tim.TotalTimeInSeconds/2),0f,1f);
        tim.enabled = false;
        InvokeRepeating(nameof(SpawnObj),1f,0.1f);
        ended = true;
    }

    public void OnLoose()
    {
        mM.MiniGameEnd(false,0);
    }
    IEnumerator WaitForFinish()
    {
        yield return new WaitForSeconds(4);
        mM.MiniGameEnd(true,multiply);
        Debug.Log("Win");
    }
}
