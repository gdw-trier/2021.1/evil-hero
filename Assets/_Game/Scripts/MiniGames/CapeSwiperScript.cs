using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = System.Random;

public class CapeSwiperScript : MonoBehaviour
{

    [SerializeField] private GameObject swipeableTile;
    [SerializeField] private Transform holder;
    [SerializeField] private int dreckmenge;

    [SerializeField] private Sprite capeLinks;
    [SerializeField] private Sprite capeRechts;
    
    
    
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private int iteratorTiles;
    public MiniGameManager mM;
    public MiniGameTimer tim;
    private float befPos;
    private bool timerNotStartet;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        iteratorTiles = 0;
        timerNotStartet = true;
    }


    // -4 2
    void Start()
    {
        SpawnSpash();
    }
    
    void Update()
    {
        if (Mouse.current.leftButton.isPressed)
        {
            rb.position = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            if (befPos > rb.position.x)
            {
                sr.sprite = capeLinks;
            }
            if (befPos < rb.position.x)
            {
                sr.sprite = capeRechts;
            }
            befPos = rb.position.x;
        }

        if (iteratorTiles <= 0)
        {
            float temp = Mathf.Clamp(tim.CurrentTime / (tim.TotalTimeInSeconds/2),0f,1f);
            mM.MiniGameEnd(true,temp);
            Debug.Log(temp);
        }
    }

    private void SpawnSpash()
    {
        for (float i = -6.7f; i <= 6.7f; i +=0.1f)
        {
            for (float j = 2.9f; j >= -2.6f; j -= 0.1f)
            {
                int ran = UnityEngine.Random.Range(0, dreckmenge);
                if (ran == 0)
                {
                    Vector2 spawnPoint = new Vector2(i, j);
                    GameObject temp = Instantiate(swipeableTile, spawnPoint, quaternion.identity,holder);
                    iteratorTiles++;
                }
            }
        }
    }
    
    public void OnLoose()
    {
        mM.MiniGameEnd(false,0);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        {
            tim.StartTimer();
            timerNotStartet = false;
        }
        Destroy(coll.gameObject);
        iteratorTiles--;
    }
}
