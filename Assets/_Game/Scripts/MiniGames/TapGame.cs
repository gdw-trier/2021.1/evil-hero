using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TapGame : MonoBehaviour
{
    private int clickCounter = 0;
    [SerializeField] private Rigidbody2D proje;
    [SerializeField] private MiniGameManager mm;
    [SerializeField] private int failurePoint = 10;
    [SerializeField] private float clickMultiplier = 2.0f;
    [SerializeField] private UnityEvent sliderStart;
    
    private bool hasCollided = false;

    
    public void Shake()
    {
        clickCounter++;
        CameraShake.Instance.ShakeCamera( 1 +  (clickCounter/50), 0.25f);

        if (clickCounter == 1)
        {
            sliderStart?.Invoke();
        }
        
        if(hasCollided)
            return;
        proje.MovePosition(proje.position + Vector2.up * (Time.deltaTime * 45.0f));
    }

    public void Kick()
    {
        if (clickCounter <= failurePoint)
            LoseHit();
        else
        {
            float multiplier = 0.5f + Mathf.Clamp(clickCounter * clickMultiplier, 0, 150.0f) / 300.0f;
            WinHit(multiplier);
        }
    }

    

    public void WinHit(float multiplier)
    {
        mm.MiniGameEnd(true, multiplier);
    }
    
    public void LoseHit()
    {
        mm.MiniGameEnd(false, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("MIEEEP");
        hasCollided = true;
    }
}
