using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Event/MiniGameEvent")]
public class MiniGameEvent : ScriptableObject
{
    public UnityAction<bool, float> OnMiniGameRequested;

    public void RaiseEvent(bool success, float multiplier)
    {
        OnMiniGameRequested?.Invoke(success, multiplier);
    }
}
