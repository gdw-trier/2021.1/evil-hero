using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    [SerializeField] private MiniGameEvent minigameEvent;
    [SerializeField] private SceneLoadEvent unloadMiniGameEvent;
    
    public void MiniGameEnd(bool successful, float multiplier)
    {
        minigameEvent.RaiseEvent(successful, multiplier);
        unloadMiniGameEvent.RaiseEvent(null);
        
    }
}
