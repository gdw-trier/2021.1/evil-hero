using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    [SerializeField] private int playerStartMoney = 10;
    [SerializeField] private int playerStartFame = 100;
    [SerializeField] private PlayerStats playerStats;
    [SerializeField] private CrimeActivHolder crimes;
    [SerializeField] private CrimeResultList crimesRes;
    private void Start()
    {
        playerStats.PlayerMoney = playerStartMoney;
        playerStats.PlayerFame = playerStartFame;
        playerStats.InitFameForDiff();
        crimes.ClearCrimes();
        crimesRes.ClearCrimes();
    }
}
