using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private SceneLoadEvent sceneToLoadAdditive;
    [SerializeField] private SceneLoadEvent sceneToUnload;
    [SerializeField] private GameObject levelToInvis;

    private string activScene = null;

    private void Awake()
    {
        sceneToLoadAdditive.OnSceneLoadRequested = LoadSceneAdditive;
        sceneToUnload.OnSceneLoadRequested = UnLoadScene;
    }

    private void LoadSceneAdditive(string sceneName)
    {
        if (SceneManager.GetSceneByName(sceneName).isLoaded)
            SceneManager.UnloadSceneAsync(sceneName);
        levelToInvis.SetActive(false);
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        activScene = sceneName;
        //sceneToLoadAdditive.OnSceneLoadRequested = null;
    }

    private void UnLoadScene(string sceneName)
    {
        if (!String.IsNullOrEmpty(activScene))
        {
            levelToInvis.SetActive(true);
            SceneManager.UnloadSceneAsync(activScene.Trim());
        }

        activScene = null;
       // sceneToUnload.OnSceneLoadRequested = null;
    }
    
    private void OnDestroy()
    {

        sceneToLoadAdditive.OnSceneLoadRequested = null;
        sceneToUnload.OnSceneLoadRequested = null;
    }

}
