using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Event/SceneLoadEvent")]
public class SceneLoadEvent : ScriptableObject
{
    public UnityAction<string> OnSceneLoadRequested;

    public void RaiseEvent(string sceneName)
    {
        OnSceneLoadRequested?.Invoke(sceneName);
    }
}
